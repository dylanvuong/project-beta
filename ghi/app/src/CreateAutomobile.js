import React, { useEffect, useState } from 'react';

function AutomobileForm(props) {
  const [color, setColor] = useState('');
  const [year, setYear] = useState(0);
  const [vin, setVin] = useState('');
  const [modelId, setModelId] = useState(0);
  const [models, setModels] = useState([]);

  const handleSubmit = async (e) => {
    e.preventDefault();

    const data = {
      color: color,
      year: year,
      vin: vin,
      model_id: modelId
    };

    const vehicleUrl = 'http://localhost:8100/api/automobiles/';
    const fetchConfig = {
      method: 'POST',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(vehicleUrl, fetchConfig);
    if (response.ok) {
      const newAutomobile = await response.json();
      console.log(newAutomobile);
      setColor('');
      setYear('0');
      setVin('');
      setModelId('0');
    }
  };

  const fetchModels = async () => {
    const modelsUrl = 'http://localhost:8100/api/models/';
    const response = await fetch(modelsUrl);
    if (response.ok) {
      const data = await response.json();
      setModels(data.models);
    }
  };

  useEffect(() => {
    fetchModels();
  }, []);

  return (
    <div className='row'>
      <div className='offset-3 col-6'>
        <div className='shadow p-4 mt-4'>
          <h1 className='text-center'>Let's create a new automobile!</h1>
          <form onSubmit={handleSubmit}>
            <div className='form-floating mb-3'>
              <input
                onChange={(event) => setColor(event.target.value)}
                className='form-control'
                value={color}
                placeholder='Enter color'
                required
                type='text'
                name='color'
                id='color'
              />
              <label htmlFor='color'>Color</label>
            </div>

            <div className='form-floating mb-3'>
              <input
                onChange={(event) => setYear(event.target.value)}
                className='form-control'
                value={year}
                placeholder='Enter year'
                required
                type='number'
                name='year'
                id='year'
              />
              <label htmlFor='year'>Year</label>
            </div>

            <div className='form-floating mb-3'>
              <input
                onChange={(event) => setVin(event.target.value)}
                className='form-control'
                value={vin}
                placeholder='Enter VIN'
                required
                type='text'
                name='vin'
                id='vin'
              />
              <label htmlFor='vin'>VIN</label>
            </div>

            <div className='mb-3'>
              <label htmlFor='modelId' className='form-label'>
                Model
              </label>
              <select
                onChange={(event) => setModelId(event.target.value)}
                required
                name='modelId'
                id='modelId'
                className='form-select'
              >
                <option value=''>Choose a Model</option>
                {models.map((model) => (
                  <option key={model.id} value={model.id}>
                    {model.name}
                  </option>
                ))}
              </select>
            </div>

            <button className='btn btn-primary' type='submit'>Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default AutomobileForm;
