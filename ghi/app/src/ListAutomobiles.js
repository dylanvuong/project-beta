import React, { useEffect, useState } from 'react';

function ListAutomobiles(props) {
  const [automobiles, setAutomobiles] = useState([]);

  useEffect(() => {
    getAutomobiles();
  }, []);

  const getAutomobiles = async () => {
    const automobileUrl = 'http://localhost:8100/api/automobiles';
    const response = await fetch(automobileUrl);
    if (response.ok) {
      const data = await response.json();
      setAutomobiles(data.autos);
    }
  };

  return (
    <table className='table table-striped'>
      <thead>
        <tr>
          <th>Color</th>
          <th>Year</th>
          <th>VIN</th>
          <th>Model</th>
        </tr>
      </thead>
      <tbody>
        {automobiles.map((automobile) => {
          return (
            <tr key={automobile.id}>
              <td>{automobile.color}</td>
              <td>{automobile.year}</td>
              <td>{automobile.vin}</td>
              <td>{automobile.model.name}</td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
}

export default ListAutomobiles;
