import React from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import VehicleForm from './CreateVehicle';
import ManufacturersList from './ManufacturersList';
import ManufacturerFormCreate from './ManufacturerForm';
import ListAutomobiles from './ListAutomobiles';

import ListTechnicians from './ListTechnicians';


import ListVehicles from './ListVehicles';
import AutomobileForm from './CreateAutomobile';
import SalespeopleList from './SalespeopleList';
import SalespeopleCreate from './SalesPeopleCreate';
import CustomerList from './CustomerList';
import CustomerCreate from './CustomerCreate';
import SalesList from './SalesList';
import SalesFormCreate from './SalesCreate';
import SalesPersonHistory from './SalesPersonHistory';
import ListManufacturers from './ListManufacturers';
import CreateManufacturer from './CreateManufacturer';
import TechnicianForm from './TechnicianForm';
import ServiceAppointmentForm from './ServiceAppointmentForm';
import ListAppointments from './ListAppointments';
import ServiceHistory from './ServiceHistory';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/models" element={<ListVehicles/>} />
          <Route path="/manufacturers/" element={<ListManufacturers />} />
          <Route path="/manufacturer/new" element={<ManufacturerFormCreate />} />
          <Route path="/technicians/new" element={<TechnicianForm />} />
          <Route path="/technicians/" element={<ListTechnicians />} />
          <Route path="/models/new" element={<VehicleForm />} />
          <Route path="/automobiles/new" element={<AutomobileForm />} />
          <Route path="/automobiles" element={<ListAutomobiles />} />
          <Route path="/salespeople" element={<SalespeopleList />} />
          <Route path="/salespeople/new" element={<SalespeopleCreate />} />
          <Route path="/customers" element={<CustomerList />} />
          <Route path="/customers/new" element={<CustomerCreate />} />
          <Route path="/sales" element={<SalesList />} />
          <Route path="/sales/new" element={<SalesFormCreate />} />
          <Route path="/sales/history" element={<SalesPersonHistory />} />
          <Route path="/manufacturers/" element={<ListManufacturers />} />
          <Route path="/manufacturers/new" element={<CreateManufacturer />} />
          <Route path="/technicians/new" element={<TechnicianForm />} />
          <Route path="/appointments/new" element={<ServiceAppointmentForm />} />
          <Route path="/appointments/" element={<ListAppointments />} />
          <Route path="/service/history/" element={<ServiceHistory />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
