import React, { useEffect, useState } from 'react';

function TechnicianList() {
  const [technicians, setTechnicians] = useState([]);

  useEffect(() => {
    fetchTechnicians();
  }, []);

  const fetchTechnicians = async () => {
    const technicianUrl = 'http://localhost:8080/api/technicians/';
    const response = await fetch(technicianUrl);
    if (response.ok) {
      const technicianData = await response.json();
      console.log(technicianData);
      setTechnicians(technicianData.technicians);
    }
  };



  return (
    <div>
      <h1>Technician List</h1>
      <table>
        <thead>
          <tr>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Employee ID</th>
          </tr>
        </thead>
        <tbody>
          {technicians.map((technician) => (
            <tr key={technician.id}>
              <td>{technician.first_name}</td>
              <td>{technician.last_name}</td>
              <td>{technician.employee_id}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default TechnicianList;
