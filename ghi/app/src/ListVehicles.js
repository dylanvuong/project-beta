import React, { useEffect, useState } from 'react';

function VehicleModelList() {
  const [models, setModels] = useState([]);
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    const fetchModels = async () => {
      const modelsUrl = 'http://localhost:8100/api/models/';
      const response = await fetch(modelsUrl);

      if (response.ok) {
        const data = await response.json();
        setModels(data.models);
      }
      setIsLoading(false);
    };

    fetchModels();
  }, []);

  if (isLoading) {
    return <div>Loading...</div>;
  }

  return (
    <div className="container">
      <h1>Vehicle Models</h1>
      {models.length === 0 ? (
        <p>No models found.</p>
      ) : (
        <ul>
          {models.map((model) => (
            <li key={model.id}>
              <h2>{model.name}</h2>
              <img src={model.picture_url} alt={model.name} />
              <h3>Manufacturer: {model.manufacturer.name}</h3>
            </li>
          ))}
        </ul>
      )}
    </div>
  );
}

export default VehicleModelList;
