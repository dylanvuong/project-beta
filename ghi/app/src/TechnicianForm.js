import React, { useState } from 'react';

function TechnicianForm(props) {
  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {
      first_name: firstName,
      last_name: lastName,
      employee_id: employeeId,
    };

    const technicianUrl = 'http://localhost:8080/api/technicians/';
    const fetchConfig = {
      method: 'POST',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(technicianUrl, fetchConfig);
    if (response.ok) {
      const newTechnician = await response.json();
      setFirstName('');
      setLastName('');
      setEmployeeId('');

    }
  };

  const [firstName, setFirstName] = useState('');
  const handleFirstNameChange = (event) => {
    const value = event.target.value;
    setFirstName(value);
  };

  const [lastName, setLastName] = useState('');
  const handleLastNameChange = (event) => {
    const value = event.target.value;
    setLastName(value);
  };

  const [employeeId, setEmployeeId] = useState('');
  const handleEmployeeIdChange = (event) => {
    const value = event.target.value;
    setEmployeeId(value);
  };

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1 className="text-center">Add a Technician</h1>
          <form onSubmit={handleSubmit}>
            <div className="form-floating mb-3">
              <input
                value={firstName}
                onChange={handleFirstNameChange}
                className="form-control"
                placeholder="Enter first name"
                required
                type="text"
                name="firstName"
                id="firstName"
              />
              <label htmlFor="firstName">First Name</label>
            </div>

            <div className="form-floating mb-3">
              <input
                value={lastName}
                onChange={handleLastNameChange}
                className="form-control"
                placeholder="Enter last name"
                required
                type="text"
                name="lastName"
                id="lastName"
              />
              <label htmlFor="lastName">Last Name</label>
            </div>

            <div className="form-floating mb-3">
              <input
                value={employeeId}
                onChange={handleEmployeeIdChange}
                className="form-control"
                placeholder="Enter employee ID"
                required
                type="text"
                name="employeeId"
                id="employeeId"
              />
              <label htmlFor="employeeId">Employee ID</label>
            </div>

            <button className="btn btn-primary" type="submit">
              Add Technician
            </button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default TechnicianForm;
