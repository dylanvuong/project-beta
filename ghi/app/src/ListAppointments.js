import React, { useState, useEffect } from 'react';

function ServiceAppointmentsList() {
  const [appointments, setAppointments] = useState([]);

  useEffect(() => {
    fetchAppointments();
  }, []);

  const fetchAppointments = async () => {
    try {
      const response = await fetch('http://localhost:8080/api/appointments');
      if (!response.ok) {
        throw new Error('Failed to fetch appointments');
      }
      const appointmentsData = await response.json();
      setAppointments(appointmentsData.appointments);
    } catch (error) {
      console.error(error);
      setAppointments([]);
    }
  };

  return (
    <div>
      <h2>Service Appointments</h2>
      {appointments.length === 0 ? (
        <p>No appointments scheduled</p>
      ) : (
        <ul>
          {appointments.map((appointment) => (
            <li key={appointment.id}>
              <div>VIN: {appointment.vin}</div>
              <div>Customer Name: {appointment.customer_name}</div>
              <div>Date and Time: {appointment.date_time}</div>
              <div>Assigned Technician: {appointment.assigned_technician}</div>
              <div>Reason: {appointment.reason}</div>
            </li>
          ))}
        </ul>
      )}
    </div>
  );
}

export default ServiceAppointmentsList;
