import React, { useState } from 'react';

function CreateCustomer(props) {
  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {
      customer_name,
      lastName,
      address,
      phoneNumber
    };

    const customerUrl = 'http://localhost:8090/api/customers/';
    const fetchConfig = {
      method: 'post',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(customerUrl, fetchConfig);
    if (response.ok) {
      const newCustomer = await response.json();
      setName('');
      setLastName('');
      setAddress('');
      setPhoneNumber('');
      props.getCustomer();
    }
  };

  const [name, setName] = useState('');
  const handleNameChange = (event) => {
    const value = event.target.value;
    setName(value);
  };

  const [lastName, setLastName] = useState('');
  const handleLastNameChange = (event) => {
    const value = event.target.value;
    setLastName(value);
  };

  const [address, setAddress] = useState('');
  const handleAddressChange = (event) => {
    const value = event.target.value;
    setAddress(value);
  };

  const [phoneNumber, setPhoneNumber] = useState('');
  const handlePhoneNumberChange = (event) => {
    const value = event.target.value;
    setPhoneNumber(value);
  };

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Add a Customer</h1>
          <form onSubmit={handleSubmit} id="create-sales_person-form">
            <div className="form-floating mb-3">
              <input
                onChange={handleNameChange}
                value={name}
                placeholder="Name"
                required
                type="text"
                name="name"
                id="name"
                className="form-control"
              />
              <label htmlFor="name">First name...</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handleLastNameChange}
                value={lastName}
                placeholder="Last name..."
                required
                type="text"
                name="lastname"
                id="lastname"
                className="form-control"
              />
              <label htmlFor="lastname">Last name...</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handleAddressChange}
                value={address}
                placeholder="Address"
                required
                type="text"
                name="address"
                id="address"
                className="form-control"
              />
              <label htmlFor="address">Address...</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handlePhoneNumberChange}
                value={phoneNumber}
                placeholder="Phone Number"
                required
                type="text" // Changed type from "number" to "text"
                name="phone_number"
                id="phone_number"
                className="form-control"
              />
              <label htmlFor="phone_number">Phone number...</label>
            </div>
            <button className="btn btn-primary">Add a Customer</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default CreateCustomer;
