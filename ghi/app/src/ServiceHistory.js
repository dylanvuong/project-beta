import React, { useState, useEffect } from 'react';

function ServiceHistory() {
  const [appointments, setAppointments] = useState([]);
  const [searchQuery, setSearchQuery] = useState('');
  const [filteredAppointments, setFilteredAppointments] = useState([]);

  useEffect(() => {
    fetchAppointments();
  }, []);

  const fetchAppointments = async () => {
    try {
      const response = await fetch('http://localhost:8080/api/appointments/');
      if (response.ok) {
        const data = await response.json();
        setAppointments(data.appointments);
        setFilteredAppointments(data.appointments); // Set filtered appointments initially
      } else {
        console.error('Error fetching appointments:', response.status);
      }
    } catch (error) {
      console.error('Error fetching appointments:', error);
    }
  };

  const handleSearchSubmit = event => {
    event.preventDefault();
    const filtered = appointments.filter(appointment =>
      appointment.vin.toLowerCase().includes(searchQuery.toLowerCase())
    );
    setFilteredAppointments(filtered);
  };

  return (
    <div>
      <h1>Service History</h1>
      <form onSubmit={handleSearchSubmit}>
        <input
          type="text"
          placeholder="Enter VIN"
          value={searchQuery}
          onChange={event => setSearchQuery(event.target.value)}
        />
        <button type="submit">Search</button>
      </form>
      <table>
        <thead>
          <tr>
            <th>VIN</th>
            <th>Customer Name</th>
            <th>Appointment Date and Time</th>
            <th>Technician ID</th>
            <th>Reason for Service</th>
            <th>Appointment Status</th>
          </tr>
        </thead>
        <tbody>
          {searchQuery.length > 0 ? (
            filteredAppointments.map(appointment => (
              <tr key={appointment.id}>
                <td>{appointment.vin}</td>
                <td>{appointment.customer_name}</td>
                <td>{appointment.date_time}</td>
                <td>{appointment.technician_id}</td>
                <td>{appointment.reason}</td>
                <td>{appointment.status}</td>
              </tr>
            ))
          ) : (
            appointments.map(appointment => (
              <tr key={appointment.id}>
                <td>{appointment.vin}</td>
                <td>{appointment.customer_name}</td>
                <td>{appointment.date_time}</td>
                <td>{appointment.technician_id}</td>
                <td>{appointment.reason}</td>
                <td>{appointment.status}</td>
              </tr>
            ))
          )}
        </tbody>
      </table>
    </div>
  );
}

export default ServiceHistory;
