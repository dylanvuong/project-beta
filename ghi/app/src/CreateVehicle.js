import React, { useEffect, useState } from 'react';

function CreateVehicle(props) {
  const [name, setName] = useState('');
  const [pictureUrl, setPictureUrl] = useState('');
  const [manufacturerId, setManufacturerId] = useState('');
  const [manufacturers, setManufacturers] = useState([]);

  const handleSubmit = async (e) => {
    e.preventDefault();

    const data = {
      name: name,
      picture_url: pictureUrl,
      manufacturer_id: manufacturerId
    };

    const vehicleUrl = 'http://localhost:8100/api/models/';
    const fetchConfig = {
      method: 'POST',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(vehicleUrl, fetchConfig);
    if (response.ok) {
      const newVehicle = await response.json();
      console.log(newVehicle);
      setName('');
      setPictureUrl('');
      setManufacturerId('');
    }
  };

  const handleChange = (event, callback) => {
    const { value } = event.target;
    callback(value);
  };

  const fetchManufacturers = async () => {
    const manufacturerUrl = 'http://localhost:8100/api/manufacturers/';

    try {
      const response = await fetch(manufacturerUrl);
      if (response.ok) {
        const data = await response.json();
        if (Array.isArray(data.manufacturers)) {
          setManufacturers(data.manufacturers);
        } else {
          console.error('Invalid manufacturers data:', data);
        }
      } else {
        console.error('Error fetching manufacturers:', response.status);
      }
    } catch (error) {
      console.error('Error fetching manufacturers:', error);
    }
  };

  useEffect(() => {
    fetchManufacturers();
  }, []);

  return (
    <div className='row'>
      <div className='offset-3 col-6'>
        <div className='shadow p-4 mt-4'>
          <h1 className='text-center'>Create a New Model</h1>
          <form onSubmit={handleSubmit}>
            <div className='form-floating mb-3'>
              <input
                onChange={(event) => handleChange(event, setName)}
                className='form-control'
                value={name}
                placeholder='name your automobile'
                required
                type='text'
                name='name'
                id='name'
              />
              <label htmlFor='name'>Model Name</label>
            </div>

            <div className='form-floating mb-3'>
              <input
                onChange={(event) => handleChange(event, setPictureUrl)}
                className='form-control'
                value={pictureUrl}
                placeholder='picture url'
                required
                type='text'
                name='picture_url'
                id='picture_url'
              />
              <label htmlFor='picture_url'>Picture URL</label>
            </div>
            <div className='mb-3'>
              <label htmlFor='manufacturer' className='form-label'>
                Manufacturer
              </label>
              <select
                onChange={(event) => handleChange(event, setManufacturerId)}
                required
                name='manufacturer'
                id='manufacturer'
                className='form-select'
                value={manufacturerId}
              >
                <option value=''>Choose a Manufacturer</option>
                {manufacturers.map((manufacturer) => (
                  <option key={manufacturer.id} value={manufacturer.id}>
                    {manufacturer.name}
                  </option>
                ))}
              </select>
            </div>
            <button className='btn btn-primary'>Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default CreateVehicle;
