import React, { useEffect, useState } from 'react';

function VehicleForm(props) {
  const [name, setName] = useState('');
  const [picture, setPicture] = useState('');
  const [manufacturer, setManufacturer] = useState('');
  const [color, setColor] = useState('');
  const [manufacturer_id, setManufacturer_id] = useState('');

  const handleSubmit = async (e) => {
    e.preventDefault();

    const data = {
      model_name: name,
      color: color,
      picture_url: picture,
      manufacturer: {
        href: manufacturer_id,
        id: parseInt(manufacturer_id.split('/').slice(-2, -1)[0]),
        name: manufacturer
      }
    };

    const vehicleUrl = 'http://localhost:8100/api/models/';
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(vehicleUrl, fetchConfig);
    if (response.ok) {
      const newVehicle = await response.json();
      console.log(newVehicle);
      setName('');
      setPicture('');
      setManufacturer('');
      setColor('');
    }
  };

  function handleChange(event, callback) {
    const { target } = event;
    const { value } = target;
    callback(value);
  }

  const handleManufacturerChange = (event) => {
    const value = event.target.value;
    const selectedManufacturer = manufacturers.find(
      (manufacturer) => manufacturer.name === value
    );
    if (selectedManufacturer) {
      setManufacturer(selectedManufacturer.name);
      setManufacturer_id(selectedManufacturer.href);
    } else {
      setManufacturer('');
      setManufacturer_id('');
    }
  };

  const [manufacturers, setManufacturers] = useState([]);
  const fetchData = async () => {
    const url = 'http://localhost:8100/api/manufacturers/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setManufacturers(data.manufacturers);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

    return (
        <div className='row'>
          <div className='offset-3 col-6'>
            <div className='shadow p-4 mt-4'>
              <h1 className="text-center">Let's create a new listing!</h1>
              <form onSubmit={handleSubmit}>
                <div className="form-floating mb-3">
                  <input
                    onChange={(event) => handleChange(event, setName)}
                    className="form-control"
                    value={name}
                    placeholder='name your automobile'
                    required
                    type="text"
                    name="name"
                    id="name"
                  />
                  <label htmlFor="name">Model Name</label>
                </div>
                <div className="form-floating mb-3">
                  <input
                    onChange={(event) => handleChange(event, setColor)}
                    className="form-control"
                    value={color}
                    placeholder='color'
                    required
                    type="text"
                    name="color"
                    id="color"
                  />
                  <label htmlFor="color">Color</label>
                </div>
                <div className="form-floating mb-3">
                  <input
                    onChange={(event) => handleChange(event, setManufacturer)}
                    className="form-control"
                    value={manufacturer}
                    placeholder='manufacturer'
                    required
                    type="text"
                    name="manufacturer"
                    id="manufacturer"
                  />
                  <label htmlFor="manufacturer">Brand Name</label>
                </div>
                <div className="form-floating mb-3">
                  <input
                    onChange={(event) => handleChange(event, setPicture)}
                    className="form-control"
                    value={picture}
                    placeholder='picture url'
                    required
                    type="text"
                    name="picture_url"
                    id="picture_url"
                  />
                  <label htmlFor="picture_url">Picture url</label>
                </div>
                <div className="mb-3">
                  <label htmlFor="bin" className="form-label">Manufacturer</label>
                  <select
                    onChange={handleManufacturerChange}
                    required
                    name="manufacturer"
                    id="manufacturer"
                    className="form-select"
                  >
                    <option value="">Choose a Manufacturer</option>
                    {manufacturers.map(manufacturer => (
                      <option key={manufacturer.name} value={manufacturer.name}>
                        {manufacturer.manufacturer_name}
                      </option>
                    ))}
                  </select>
                </div>
                <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
      );
    }


    export default VehicleForm;
