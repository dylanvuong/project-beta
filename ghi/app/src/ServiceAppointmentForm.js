import React, { useState, useEffect } from 'react';

function ServiceAppointmentForm(props) {
  const [vin, setVin] = useState('');
  const [customerName, setCustomerName] = useState('');
  const [dateTime, setDateTime] = useState('');
  const [assignedTechnician, setAssignedTechnician] = useState('');
  const [reason, setReason] = useState('');
  const [technicians, setTechnicians] = useState([]);
  const [appointments, setAppointments] = useState([]);

  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {
      vin: vin,
      customer_name: customerName,
      date_time: dateTime,
      assigned_technician: assignedTechnician,
      reason: reason,
    };

    const appointmentUrl = 'http://localhost:8080/api/appointments/new';
    const fetchConfig = {
      method: 'POST',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    try {
      const response = await fetch(appointmentUrl, fetchConfig);

      if (response.ok) {
        const newAppointment = await response.json();
        setVin('');
        setCustomerName('');
        setDateTime('');
        setAssignedTechnician('');
        setReason('');

      } else {
        throw new Error('Failed to create service appointment');
      }
    } catch (error) {
      console.error(error);
    }
  };

  const fetchTechnicians = async () => {
    try {
      const response = await fetch('http://localhost:8080/api/technicians');
      if (!response.ok) {
        throw new Error('Failed to fetch technicians');
      }
      const techniciansData = await response.json();
      setTechnicians(techniciansData.technicians);
    } catch (error) {
      console.error(error);
      setTechnicians([]);
    }
  };


  const fetchAppointments = async () => {
    try {
      const response = await fetch('http://localhost:8080/api/appointments');
      if (!response.ok) {
        throw new Error('Failed to fetch appointments');
      }
      const appointmentsData = await response.json();
      setAppointments(appointmentsData.appointments);
    } catch (error) {
      console.error(error);
      setAppointments([]);
    }
  };

  useEffect(() => {
    fetchTechnicians();
    fetchAppointments();
  }, []);

  return (
    <div>
      <h1>Create a Service Appointment</h1>

      <form onSubmit={handleSubmit}>
        <div>
          <label htmlFor="vin">VIN:</label>
          <input
            type="text"
            id="vin"
            value={vin}
            onChange={(e) => setVin(e.target.value)}
            required
          />
        </div>

        <div>
          <label htmlFor="customerName">Customer Name:</label>
          <input
            type="text"
            id="customerName"
            value={customerName}
            onChange={(e) => setCustomerName(e.target.value)}
            required
          />
        </div>

        <div>
          <label htmlFor="dateTime">Date and Time:</label>
          <input
            type="datetime-local"
            id="dateTime"
            value={dateTime}
            onChange={(e) => setDateTime(e.target.value)}
            required
          />
        </div>

        <div>
          <label htmlFor="assignedTechnician">Assigned Technician:</label>
          <select
            id="assignedTechnician"
            value={assignedTechnician}
            onChange={(e) => setAssignedTechnician(e.target.value)}
            required
          >
            <option value="">Choose a Technician</option>
            {technicians.map((technician) => (
              <option key={technician.id} value={technician.id}>
                {technician.first_name} {technician.last_name}
              </option>
            ))}
          </select>
        </div>

        <div>
          <label htmlFor="reason">Reason:</label>
          <textarea
            id="reason"
            value={reason}
            onChange={(e) => setReason(e.target.value)}
            required
          ></textarea>
        </div>

        <button type="submit">Create Appointment</button>
      </form>


    </div>
  );
}

export default ServiceAppointmentForm;
