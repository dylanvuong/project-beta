import React, { useEffect, useState } from 'react';

function ManufacturerList() {
  const [manufacturers, setManufacturers] = useState([]);


  useEffect(() => {
    const fetchManufacturers = async () => {
      try {
        const response = await fetch('http://localhost:8100/api/manufacturers/');
        if (response.ok) {
          const data = await response.json();
          setManufacturers(data);
        } else {
          console.error('Failed to fetch manufacturers:', response.status);
        }
      } catch (error) {
        console.error('Error fetching manufacturers:', error);
      }
    };

    fetchManufacturers();
  }, []);


  const deleteManufacturer = async (manufacturerId) => {
    try {
      const response = await fetch(`http://localhost:8100/api/manufacturers/${manufacturerId}`, {
        method: 'DELETE',
      });
      if (response.ok) {

        setManufacturers((prevManufacturers) =>
          prevManufacturers.filter((manufacturer) => manufacturer.id !== manufacturerId)
        );
        console.log('Manufacturer deleted successfully');
      } else {
        console.error('Failed to delete manufacturer:', response.status);
      }
    } catch (error) {
      console.error('Error deleting manufacturer:', error);
    }
  };

  return (
    <div>
      <h1>Manufacturer List</h1>
      {manufacturers.map((manufacturer) => (
        <div key={manufacturer.id}>
          <span>{manufacturer.name}</span>
          <button onClick={() => deleteManufacturer(manufacturer.id)}>Delete</button>
        </div>
      ))}
    </div>
  );
}

export default ManufacturerList;
