import React, { useEffect, useState } from 'react';

function ListManufacturers(props) {
  const [manufacturers, setManufacturers] = useState([]);
  const [isLoading, setIsLoading] = useState(true);

  const fetchManufacturers = async () => {
    const manufacturerUrl = 'http://localhost:8100/api/manufacturers/';
    const response = await fetch(manufacturerUrl);

    if (response.ok) {
      const data = await response.json();
      setManufacturers(data.manufacturers);
    }
    setIsLoading(false);
  };

  const deleteManufacturer = async (manufacturer) => {
    const manufacturerUrl = `http://localhost:8100/api/manufacturers/${manufacturer.id}/`;
    const fetchConfig = {
      method: "DELETE",
    };

    const response = await fetch(manufacturerUrl, fetchConfig);
    if (response.ok) {
      fetchManufacturers();
    }
  };

  useEffect(() => {
    fetchManufacturers();
  }, []);

  if (isLoading) {
    return <div>Loading...</div>; // Render a loading state while fetching data
  }

  return (
    <div className="container">
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Name</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          {manufacturers.map(manufacturer => (
            <tr key={manufacturer.id}>
              <td>{manufacturer.name}</td>
              <td>
                <button className="btn btn-danger" onClick={() => deleteManufacturer(manufacturer)}>Delete</button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default ListManufacturers;
