import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <div className="row">
              <div className="col-6 col-md-1">
                <li className="nav-item" style={{ marginBottom: '10px' }}>
                  <NavLink className="nav-link" aria-current="page" to="/manufacturers">Manufacturers</NavLink>
                </li>
                <li className="nav-item" style={{ marginBottom: '10px' }}>
                  <NavLink className="nav-link" aria-current="page" to="/manufacturer/new">Create Manufacturer</NavLink>
                </li>
              </div>
              <div className="col-6 col-md-1">
                <li className="nav-item" style={{ marginBottom: '10px' }}>
                  <NavLink className="nav-link" activeclassname="active" to="/models">Vehicle Models</NavLink>
                </li>
                <li className="nav-item" style={{ marginBottom: '10px' }}>
                  <NavLink className="nav-link" activeclassname="active" to="/models/new">Create Vehicle Model</NavLink>
                </li>
              </div>
              <div className="col-6 col-md-1">
                <li className="nav-item" style={{ marginBottom: '10px' }}>
                  <NavLink className="nav-link" aria-current="page" to="/automobiles">Automobiles</NavLink>
                </li>
                <li className="nav-item" style={{ marginBottom: '10px' }}>
                  <NavLink className="nav-link" aria-current="page" to="/automobiles/new">Create Automobile</NavLink>
                </li>
              </div>
              <div className="col-6 col-md-1">
                <NavLink className="nav-link" aria-current="page" to="/salespeople">Salespeople</NavLink>
                <NavLink className="nav-link" aria-current="page" to="/salespeople/new">Add a Salesperson</NavLink>
              </div>
              <div className="col-6 col-md-1">
                <NavLink className="nav-link" aria-current="page" to="/customers">Customers</NavLink>
                <NavLink className="nav-link" aria-current="page" to="/customers/new">Add a Customer</NavLink>
              </div>
              <div className="col-6 col-md-1">
                <NavLink className="nav-link" aria-current="page" to="/sales">Sales</NavLink>
                <NavLink className="nav-link" aria-current="page" to="/sales/new">Add a Sale</NavLink>
              </div>
              <div className="col-6 col-md-1">
                <NavLink className="nav-link" aria-current="page" to="/sales/history">Salesperson History</NavLink>
              </div>
              <div className="col-6 col-md-1">
                <li className="nav-item" style={{ marginBottom: '10px' }}>
                  <NavLink className="nav-link" activeclassname="active" to="/technicians">Technicians</NavLink>
                </li>
                <li className="nav-item" style={{ marginBottom: '10px' }}>
                  <NavLink className="nav-link" activeclassname="active" to="/technicians/new">Add Technician</NavLink>
                </li>
              </div>
              <div className="col-6 col-md-1">
                <li className="nav-item" style={{ marginBottom: '10px' }}>
                  <NavLink className="nav-link" activeclassname="active" to="/appointments/new">Create Appointment</NavLink>
                </li>
              </div>
              <div className="col-6 col-md-1">
                <NavLink className="nav-link" activeclassname="active" to="/appointments">Service Appointments</NavLink>
              </div>
              <div className="col-6 col-md-1">
                <li className="nav-item" style={{ marginBottom: '10px' }}>
                  <NavLink className="nav-link" activeclassname="active" to="/service/history">Service History</NavLink>
                </li>
              </div>
            </div>
          </ul>
        </div>
      </div>
    </nav>
  );
}

export default Nav;
