from django.db import models

class Technician(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    employee_id = models.CharField(max_length=100)

    def __str__(self):
        return f"{self.first_name} {self.last_name}"

class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    sold = models.BooleanField(default=False)
    import_href = models.CharField(max_length=200, null=True)

    def __str__(self):
        return self.vin

class Appointment(models.Model):
    date_time = models.DateTimeField()
    reason = models.CharField(max_length=100)
    status = models.CharField(max_length=100)
    vin = models.CharField(max_length=17)  # Assuming the VIN length is 17 characters
    customer_name = models.CharField(max_length=100)
    technician = models.ForeignKey(Technician, on_delete=models.CASCADE)

    def __str__(self):
        return f"Appointment #{self.pk}"
