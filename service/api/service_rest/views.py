import json
from django.shortcuts import get_object_or_404
from django.http import JsonResponse
from .models import Technician, Appointment
from common.json import ModelEncoder


class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        "id",
        "first_name",
        "last_name",
        "employee_id",
    ]

class AppointmentEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "id",
        "date_time",
        "reason",
        "status",
        "vin",
        "customer_name",
        "technician_id",
    ]

def api_list_technicians(request):
    technicians = Technician.objects.all()
    technician_data = [TechnicianEncoder().default(technician) for technician in technicians]
    return JsonResponse({'technicians': technician_data})

def api_create_technician(request):
    content = json.loads(request.body)
    technician = Technician.objects.create(
        first_name=content['first_name'],
        last_name=content['last_name'],
        employee_id=content['employee_id']
    )
    technician_data = TechnicianEncoder().default(technician)
    return JsonResponse({'technician': technician_data})

def api_delete_technician(request, id):
    technician = get_object_or_404(Technician, pk=id)
    technician.delete()
    return JsonResponse({'message': 'Technician deleted successfully'})

def api_list_appointments(request):
    appointments = Appointment.objects.all()
    appointment_data = [AppointmentEncoder().default(appointment) for appointment in appointments]
    return JsonResponse({'appointments': appointment_data})

def api_create_appointment(request):
    content = json.loads(request.body)
    appointment = Appointment.objects.create(
        date_time=content['date_time'],
        reason=content['reason'],
        vin=content['vin'],
        customer_name=content['customer_name'],
        technician_id=content['assigned_technician']
    )
    return JsonResponse({'id': appointment.id})

def api_delete_appointment(request, id):
    appointment = get_object_or_404(Appointment, pk=id)
    appointment.delete()
    return JsonResponse({'message': 'Appointment deleted successfully'})

def api_cancel_appointment(request, id):
    appointment = get_object_or_404(Appointment, pk=id)
    appointment.status = 'canceled'
    appointment.save()
    return JsonResponse({'message': 'Appointment status set to canceled'})

def api_finish_appointment(request, id):
    appointment = get_object_or_404(Appointment, pk=id)
    appointment.status = 'finished'
    appointment.save()
    return JsonResponse({'message': 'Appointment status set to finished'})

def get_technicians(request):
    technicians = Technician.objects.all()
    technician_data = [
        {
            'id': technician.id,
            'first_name': technician.first_name,
            'last_name': technician.last_name,
            'employee_id': technician.employee_id,
        }
        for technician in technicians
    ]
    return JsonResponse({'technicians': technician_data})
