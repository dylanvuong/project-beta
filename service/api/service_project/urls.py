from django.urls import path
from service_rest.views import (
    api_list_technicians,
    api_create_technician,
    api_delete_technician,
    api_list_appointments,
    api_create_appointment,
    api_delete_appointment,
    api_cancel_appointment,
)

urlpatterns = [
    path("api/technicians/", api_list_technicians),
    path("api/technicians/new", api_create_technician),
    path("api/technicians/<int:id>/", api_delete_technician),
    path("api/appointments/", api_list_appointments),
    path("api/appointments/new", api_create_appointment),
    path("api/appointments/<int:id>/", api_delete_appointment),
    path("api/appointments/<int:id>/cancel/", api_cancel_appointment),
]
